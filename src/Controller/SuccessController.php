<?php

namespace Drupal\trinion_cart_just_invoice\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

class SuccessController extends ControllerBase {
    public function build() {
        $build['#markup'] = t('We will contact you as soon as possible.');
        return $build;
    }
}
